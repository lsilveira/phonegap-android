
// common functions
function getUserParams() {
  var intermediary_field  = document.getElementById("intermediary_id");
  var intermediary_id     = intermediary_field.options[intermediary_field.selectedIndex].value;
  var email               = document.getElementById("email").value;
  var password            = document.getElementById("password").value;    

  var params = {intermediary_id: intermediary_id, email: email, password: password};
  return params;
}

function errorCB(err) {
  alert("Error processing SQL: "+err.code);
}

// index
function querySuccess(tx, results) {  
  var view = document.getElementById("view");
  var html = new EJS({url: 'ejs_templates/users_list.ejs'}).render({results: results});
  view.innerHTML = html;  
}

function indexUsers() {
  var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);

  db.transaction(function(tx) {
    tx.executeSql('SELECT * FROM users', [], querySuccess);
  }, errorCB);
}


// new
function newUser() {
  user = {intermediary_id:"", email:"", password:""}
  var html = new EJS({url: 'ejs_templates/users_form.ejs'}).render({title: "Novo Usuario", user: user, btValue: "Inserir", btFunction: "createUser();"});
  view.innerHTML = html; 
}


// edit
function editUserForm(tx, results) {     
  var view = document.getElementById("view");  
  var user = results.rows.item(0);  
  var user = results.rows.item(0);  

  var html = new EJS({url: 'ejs_templates/users_form.ejs'}).render({title: "Editar Usuario", user: user, btValue: "Atualizar", btFunction: "updateUser("+ user.id +");"});
  view.innerHTML = html;  
}

function editUser(user_id) {  
  var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
  
  db.transaction(function(tx) {
    tx.executeSql("SELECT * FROM users WHERE id=?", [user_id], editUserForm);

  }, errorCB);
}


// create
function createUser() {
  var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);         

  db.transaction(function(tx) {
    var params = getUserParams();   
  
    tx.executeSql('CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, intermediary_id INTEGER NOT NULL, email VARCHAR(250) NOT NULL, password VARCHAR(250));');
    tx.executeSql('INSERT INTO users VALUES (NULL,?,?,?)', [params.intermediary_id, params.email, params.password]);

    var message = document.getElementById("message");
    message.style.display = "block";
    message.innerHTML = "Usuario inserido com sucesso";

  }, errorCB);
}


// update
function updateUser(user_id) {  
  user = getUserParams();
  
  var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
  
  db.transaction(function(tx) {    
    tx.executeSql("UPDATE users SET email=?, password=?, intermediary_id=? WHERE id=?",[user.email, user.password, user.intermediary_id, user_id]);    

    var message = document.getElementById("message");
    message.style.display = "block";    
    message.innerHTML = "Usuario atualizado com sucesso";

    // // editUser(user_id);

  }, errorCB);
}


// destroy
function destroyUser(user_id) {
  var db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
  
  db.transaction(function(tx) {
    tx.executeSql("DELETE FROM users WHERE id=?", [user_id]);
    
    var message = document.getElementById("message");
    message.style.display = "block";
    message.innerHTML = "Usuario removido.";    

    indexUsers();

  }, errorCB);
}


// Intermediaries select data
intermediaries = [{ name:"Joao Carlos", value:1 }, { name:"Marcos Paulo", value:2 }, { name:"Claudia Abreu", value:3 }];  